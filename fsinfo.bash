#!/bin/bash

# tar imot kommandolinje argument

if [ "$1" != "" ] 
then
# setter path:
path=$(locate -b "$1")
# dersom dir eksisterer:
	if [ -d "$path" ] 
	then
# sett path eller exit
		cd "$path" || exit
# setter parameterne:
		partisjon=$(df -h . | awk '{$6}')
		antFiler=$(sudo find . -type f -print | wc -l)
		totStr=$(wc -c -- *glob*) 		# deles p� antFiler for gje
		gjeStr=$((antFiler / totStr))
		pathMaxFil=$(du -a / | sort -n -r | head -n 1 | awk '{$2}')
		maxFil=$(du -a / | sort -n -r | head -n 1 | awk '{$1}')
		flestHardlink=$(find . -ld | sort -n -r | head -n 1 | awk '$8')
		antHardlink=$(stat -c %h)
		
		echo "Partisjoen /$1/ befinner seg p� er $partisjon full" 
		echo "Det finnes $antFiler filer"
		echo "Den st�rste er $pathMaxFil som er $maxFil stor."
		echo "Gjennomsnittlig filst�rrelse er ca. $gjeStr bytes"
		echo "Filen $flestHardlink har flest hardlinks, den har $antHardlink"
	else
		echo "angitt path eksisterer ikke ...programmet avsluttes"
     	fi
# feilmelding dersom dir ikke er angitt    
else
	echo "Vennligst angi et dir ...programmet avsluttes" 
fi