#!/bin/bash
# no arguments, display a menu with option for info
# about processes

clear
while [ "$svar" != "9" ]
do
 echo ""
 echo "  1 - Hvem er jeg?"
 echo "  2 - Hvor lenge er det siden siste boot?"
 echo "  3 - Hva er gjennomsnittlig load siste minutt?"
 echo "  4 - Hvor mange prosesser og tråder finnes?"
 echo "  5 - Hvor mange context switch'er fant sted siste sekund?"
 echo "  6 - Hvor mange interrupts fant sted siste sekund?"
 echo "  9 - Avslutt dette scriptet"
 echo ""
 echo -n "Velg en funksjon: "
 read -r svar
 echo ""
 
 case $svar in
  1)clear
    echo "Jeg er $(whoami)"
    read -r
    clear
    ;;
  2)clear
    echo "Siste boot ble gjenomfort $(uptime -s)"
    read -r
    clear
    ;;
  3)clear
  echo "Gjennomsnittlig load siste minutt var $(uptime | awk '{print $9}')"
  read -r
  clear
  ;;
  4)clear
  echo "Det finnes $(ps -e | wc -l) prosesser og $(ps axms | wc -l) traader"
  read -r
  clear
  ;;
  5)clear
  echo "Antall Context switcher'er som fant sted siste sekund var $(pidstat -w | awk '{print $6}' | wc -l)"
  read -r
  clear
  ;;
  6)clear
  echo "Antall interrupts i CPU siste sekund var $(mpstat -I CPU | wc -l)"
  read -r
  clear
  ;;
  9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg
    read -r
    clear
    ;;
 esac
done

