#!/bin/bash

# Array som inneholder processId
chromePid=$(pgrep chrome)
	
# for number of elements in the array, do:

	for n in "${chromePid[@]}"
	do
		mjPF=$(ps --no-headers -o maj_flt "$n")
		
		echo "Chrome $n har forårsaket:$mjPF major page faults" 
		
		if [[ $mjPF -gt 1000 ]]
		then
			printf "\t(mer enn 1000!)\n\n"
		fi
done