#!/bin/bash

# dersom kommandoarg ikke eksisterer: feilmelding
if [ $# == "" ]
then
	printf "Feilmelding... Navn p� prosess er ikke angitt. ...Programmet avsluttes"

else
	# Loop gjennom alle prosessene
	while [ "$1" != "" ]
	do
	    
	    # opretter en fil (PId-����mmdd-hh\:mm\:ss.bash) og leser data
	   
	    cat << EOT > "$#-$(date '%y%m%d-%H\:%M\:%S').bash"
	    
	    # Henter prosessdata: 
	    Vmsize=$("/proc/[$#]/status/VmSize")
	    VmData=$("/proc/[$#]/status/VmData")
	    VmStk=$("/proc/[$#]/status/VmStk")
	    VmExe=$("/proc/[$#]/status/VmExe")
	    privatVm=$("$VmData" + "$VmStk" + "$VmExe")
	    VmLib=$("/proc/[$#]/status/VmLib")
	    VmRss=$("/proc/[$#]/status/VmRss")
	    VmPTE=$("/proc/[$#]/status/VmPTE")
	    
	    echo "******** Minne info om prosess med PID "$#" ********"
	    echo "Total bruk av virtuelt minne (VmSize):\t "$VmSize""
	    echo "Mengde privat virtuelt minne (VmData+VmStk+VmExe):\t "$PrivatVm""
	    echo "Mengde shared virtuelt minne (VmLib):\t "$VmLib""
	    echo "Total bruk av fysisk minne(VmRSS):\t "$VmRSS""
	    echo "Mengde fysisk minne som benyttes til page table(VmPTE):\t "$VmPTE"\n\n\n"
EOT
	
	    # Shift all the parameters down by one
	    shift
	
done
fi